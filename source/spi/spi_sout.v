//-----------------------------------------------------------------------------
//	file: spi_sout.v
//	breif:
//		数据输出模块
//-----------------------------------------------------------------------------
module spi_sout #(
	parameter	DATA_WIDTH = 16
)
(
	input		clk,
	input		reset,
	input		load,
	input		shif_en,
	output		mosi,
	input		[DATA_WIDTH-1:0] pout
);

// register
reg		[DATA_WIDTH-1:0] pout_buf;

// spi clock
always @(posedge clk or negedge reset)
	if (reset == 1'b0)
		pout_buf <= {DATA_WIDTH{1'b0}};
	else if (load == 1'b1)
		pout_buf <= pout;
	else if (shif_en == 1'b1)
		pout_buf <= {pout_buf[DATA_WIDTH-2:0], 1'b0};

// spi out
assign	mosi = pout_buf[DATA_WIDTH-1];

endmodule
