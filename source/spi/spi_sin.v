//-----------------------------------------------------------------------------
//	file: spi_sin.v
//	breif:
//		数据输入模块
//-----------------------------------------------------------------------------
module spi_sin #(
	parameter	DATA_WIDTH = 16
)
(
	input		clk,
	input		reset,
	input		samp_en,
	input		miso,
	output	reg	[DATA_WIDTH-1:0] pin
);

// spi clock
always @(posedge clk or negedge reset)
	if (reset == 1'b0)
		pin <= {DATA_WIDTH{1'b0}};
	else if (samp_en == 1'b1)
		pin <= {pin[DATA_WIDTH-2:0], miso};

endmodule
