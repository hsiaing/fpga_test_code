//-----------------------------------------------------------------------------
//	file: ad5424.v
//	brief:
//		single channel, update per 100ns
//-----------------------------------------------------------------------------
module ad5424 (
	input		clk,
	input		reset,
	input		start,
	output		rwn,
	output		csn,
	output		stop
);

// parameter
parameter	CNT_NUM = 4'd9;

// register
reg		[3:0] cnt;

// counter
always @(posedge clk or negedge reset)
	if (reset == 1'b0)
		cnt <= 4'd0;
	else if (start == 1'b0)
		cnt <= 4'd0;
	else if (cnt == CNT_NUM)
		cnt <= 4'd0;
	else
		cnt <= cnt + 1'b1;

// write
assign rwn = ((cnt <= 4'd8) && (cnt >= 4'd3)) ? 1'b0 : 1'b1;
// chip slect
assign csn = rwn;
// write complete
assign stop = (cnt == CNT_NUM) ? 1'b1 : 1'b0;

endmodule
