//-----------------------------------------------------------------------------
//	file: top.v
//		top of project
//-----------------------------------------------------------------------------
module top (
	input		clk,
	input		reset,
	input		enable,
	output		wr_mem_en
);

// wire
wire	clk_sys;
wire	reset_sys;
wire	rd_mem_en;
wire	[9:0] pin;
wire	nss;
wire	sck;
wire	link;

// pll & reset
clk_module clk_module_inst (
	.clk		(clk),
	.reset		(reset),
	.clk_sys	(clk_sys),
	.reset_sys	(reset_sys)
);

// spi module
spi_module #(
	.DATA_WIDTH	(10)
)
spi_module_inst (
	.clk		(clk_sys),
	.reset		(reset_sys),
	.enable		(enable),
	.rd_mem_en	(rd_mem_en),
	.wr_mem_en	(wr_mem_en),
	.pout		(10'h3DD),
	.pin		(pin),
	.nss		(nss),
	.sck		(sck),
	.miso		(link),
	.mosi		(link)
);

endmodule
