//-----------------------------------------------------------------------------
//	file: shift.v
//	author:	hsia
//		find 11001110 using shift
//-----------------------------------------------------------------------------
module shift (
	input		clk,
	input		reset,
	input		din,
	output	reg	shift_out
);

// register
reg		[7:0] data;

// shift
always @(posedge clk or negedge reset) begin
	if (reset == 1'b0) begin
		data <= 8'd0;
	end
	else begin
		data <= {din, data[7:1]};
	end
end

// shift out
always @(data) begin
	if (data == 8'b1100_1110) begin
		shift_out <= 1'b1;
	end
	else begin
		shift_out <= 1'b0;
	end
end

endmodule
