//-----------------------------------------------------------------------------
//	file: fsm.v
//	author:	hsia
//		find 11001110 using fsm
//-----------------------------------------------------------------------------
module fsm (
	input		clk,
	input		reset,
	input		din,
	output	reg	fsm_out
);

// parameter
parameter	S0 = 8'b0000_0001;
parameter	S1 = 8'b0000_0010;
parameter	S2 = 8'b0000_0100;
parameter	S3 = 8'b0000_1000;
parameter	S4 = 8'b0001_0000;
parameter	S5 = 8'b0010_0000;
parameter	S6 = 8'b0100_0000;
parameter	S7 = 8'b1000_0000;

// register
reg		[7:0] state;

// fsm
always @(posedge clk or negedge reset) begin
	if (reset == 1'b0) begin
		state <= S0;
	end
	else begin
		case (state)
			S0: begin // 0
				if (din == 1'b1)
					state <= S0;
				else 
					state <= S1;
			end
			S1: begin // 1
				if (din == 1'b1)
					state <= S2;
				else 
					state <= S1;
			end
			S2: begin // 1
				if (din == 1'b1)
					state <= S3;
				else 
					state <= S1;
			end
			S3: begin // 1
				if (din == 1'b1)
					state <= S4;
				else
					state <= S1;
			end
			S4: begin // 0
				if (din == 1'b1)
					state <= S0;
				else
					state <= S5;
			end
			S5: begin // 0
				if (din == 1'b1)
					state <= S2;
				else
					state <= S6;
			end
			S6: begin // 1
				if (din == 1'b1)
					state <= S7;
				else
					state <= S1;
			end
			S7: begin // 1
				if (din == 1'b1)
					state <= S0;
				else
					state <= S1;
			end
		endcase
	end
end

// fsm out
always @(posedge clk or negedge reset) begin
	if (reset == 1'b0) begin
		fsm_out <= 1'b0;
	end
	else if ((state == S7) && (din == 1'b1)) begin
		fsm_out <= 1'b1;
	end
	else begin
		fsm_out <= 1'b0;
	end
end

endmodule
