//-----------------------------------------------------------------------------
//	file: sedge.v
//	breif:
//		边沿检测，提供跳变沿、上升沿、下降沿三种信号。只用于系统信号检测，外部信号
//	检测要先进行同步。
//-----------------------------------------------------------------------------
module sedge (
	input		clk,
	input		sin,
	output		new,
	output		pos,
	output		neg
);

// register
reg		sbuf = 1'b0;

// buffer
always @(posedge clk)
	sbuf <= sin;

// edge
assign new = sin ^ sbuf;
assign pos = sin & (~sbuf);
assign neg = (~sin) & sbuf;

endmodule
