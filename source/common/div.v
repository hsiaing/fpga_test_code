//-----------------------------------------------------------------------------
//	file: div.v
//	breif:
//		分频模块，提供时钟信号，CNT_CYCLE和CNT_HALF的数值应该减一，例如，16分频的话
//	CNT_HALF的值应该为4'd7，CNT_CYCLE的值应该为4'd15。
//-----------------------------------------------------------------------------
module div #(
	parameter	CNT_WIDTH = 4,
	parameter	CNT_HALF = 4'd7,
	parameter	CNT_CYCLE = 4'd15
)
(
	input		clk,
	input		reset,
	output	reg	clk_out,
	output	reg	clk_pos,
	output	reg	clk_neg
);

// register
reg		[(CNT_WIDTH-1):0] cnt;

// counter
always @(posedge clk or negedge reset)
	if (reset == 1'b0)
		cnt <= {CNT_WIDTH{1'b0}};
	else if (cnt == CNT_CYCLE)
		cnt <= {CNT_WIDTH{1'b0}};
	else
		cnt <= cnt + 1'b1;

// clock out
always @(posedge clk or negedge reset)
	if (reset == 1'b0)
		clk_out <= 1'b0;
	else if (cnt == CNT_HALF)
		clk_out <= 1'b1;
	else if (cnt == CNT_CYCLE)
		clk_out <= 1'b0;

// clock pos edge
always @(posedge clk or negedge reset)
	if (reset == 1'b0)
		clk_pos <= 1'b0;
	else if (cnt == CNT_HALF)
		clk_pos <= 1'b1;
	else
		clk_pos <= 1'b0;

// clock neg edge
always @(posedge clk or negedge reset)
	if (reset == 1'b0)
		clk_neg <= 1'b0;
	else if (cnt == CNT_CYCLE)
		clk_neg <= 1'b1;
	else
		clk_neg <= 1'b0;

endmodule
