//-----------------------------------------------------------------------------
//	file: sync.v
//	breif:
//		用于同步外部信号，两步寄存，第一次用于同步，第二次用于消除亚稳态
//-----------------------------------------------------------------------------
module sync (
	input		clk,
	input		sin,
	output	reg	sout
);

// register
reg		sbuf = 1'b0;

// signal synchronization
always @(posedge clk)
	sbuf <= sin;

// eliminate metastable
always @(posedge clk)
	sout <= sbuf;

endmodule
