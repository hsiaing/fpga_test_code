//-----------------------------------------------------------------------------
//	file: clk_module.v
//	breif:
//		整合了PLL和RESET的模块,使输出倍频时钟和复位稳定，异步复位，同步释放
//-----------------------------------------------------------------------------
module clk_module (
	input		clk,
	input		reset,
	output		clk_sys,
	output		reset_sys
);

// wire
wire		locked;

// pll
pll	pll_inst (
	.inclk0		(clk),
	.c0			(clk_sys),
	.locked		(locked)
);

// reset synchronization
sync reset_inst (
	.clk		(clk),
	.sin		(reset & locked),
	.sout		(reset_sys)
);

endmodule
