//-----------------------------------------------------------------------------
//	file: rom_module.v
//	breif:
//		生成正弦波
//-----------------------------------------------------------------------------
module rom_module (
	input		clk,
	input		reset,
	input		rd,
	output		[7:0] dat
);

// register
reg			[8:0] addr;

// address add
always @(posedge clk or negedge reset)
	if (reset == 1'b0)
		addr <= 9'd0;
	else if (rd == 1'b1)
		addr <= addr + 1'b1;

// rom
rom_8x512 rom_8x512_inst (
	.address	(addr),
	.clock		(clk),
	.q			(dat)
);

endmodule
