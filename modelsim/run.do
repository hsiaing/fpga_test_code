# 退出并清理
quit	-sim
.main	clear

# 建立库并映射
vlib	work
vmap	work work

# 编译
vlog	-work work ./tb_top.v
vlog	-work work ./simlib/*.v
# ----
vlog	-work work ../source/*.v
vlog	-work work ../source/ad5424/*.v
vlog	-work work ../source/clock/*.v
vlog	-work work ../source/common/*.v
vlog	-work work ../source/rom/*.v
vlog	-work work ../source/spi/*.v

# 仿真
vsim	-voptargs=+acc work.tb_top

# 添加波形
# add wave	-divider {****}
# add wave	tb_top/top_inst/*
add wave	-divider {****}
add wave	tb_top/top_inst/spi_module_inst/*
# add wave	-divider {****}
# add wave	-radix unsigned		tb_top/top_inst/spi_module_inst/spi_clk_inst/*
# add wave	-divider {****}
# add wave	tb_top/top_inst/spi_module_inst/spi_fsm_inst/*
# add wave	-radix unsigned -analog-step -height 256 -max 256		tb_top/top_inst/dat

# 设置运行时间
run 3us

# Zoom the wave display to show the full simulation time
# wave zoom full

# Sets left and right edge of wave display to the specified start time and end time
wave zoom range 200ns 1100ns
