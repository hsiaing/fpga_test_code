//-----------------------------------------------------------------------------
//	file: tb_top.v
//		testbech of project
//-----------------------------------------------------------------------------
`timescale	1ns/1ns
module tb_top;

// port
reg		clk;
reg		reset;
reg		enable;
wire	wr_mem_en;

// register
reg		[1:0] cnt = 2'd0;

// inst
top top_inst (
	.clk		(clk),
	.reset		(reset),
	.enable		(enable),
	.wr_mem_en	(wr_mem_en)
);

// clock
initial begin
	clk = 0;
	forever #15 clk <= ~clk;
end

// reset
initial begin
	reset = 0;
	#200;
	reset = 1;
end

// enable
initial begin
	enable = 1;
end

always @(wr_mem_en)
	if (wr_mem_en)
		cnt <= cnt + 1'b1;

always @(cnt)
	if (cnt == 2'd3)
		enable <= 0;

endmodule
